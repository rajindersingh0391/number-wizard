﻿using UnityEngine;
using TMPro;

public class NumberWizard : MonoBehaviour
{
    [SerializeField] int minVal;
    [SerializeField] int maxVal;

    [SerializeField] TextMeshProUGUI guessText;

    int guess;

    // Start is called before the first frame update
    void Start()
    {
        NextGuess();
    }

    public void OnClickLower()
    {
        maxVal = guess - 1;
        NextGuess();
    }

    public void OnClickHigher() 
    {
        minVal = guess + 1;
        NextGuess();
    }

    void NextGuess() {
        guess = Random.Range(minVal, maxVal + 1);
        guessText.text = guess.ToString();
    }
}
